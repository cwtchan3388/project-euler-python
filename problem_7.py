"""Projet Euler problem 7, 10001st prime"""
from helpers.maths import is_prime
from helpers.timers import timer


@timer
def solution(nth_prime_number: int) -> int:
    """Finds the nth prime number"""
    prime_number = 0
    cur_number = 1
    while True:
        if is_prime(cur_number):
            prime_number += 1
            if prime_number == nth_prime_number:
                break
        cur_number += 1

    return cur_number


if __name__ == "__main__":
    print(solution(10001))
