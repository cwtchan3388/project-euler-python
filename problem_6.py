"""Project Euler problem 6
Sum square difference
"""
import math

import helpers.maths
from helpers.timers import timer


@timer(10)
def solution(n: int) -> int:
    return int(math.pow(helpers.maths.sum_n(n), 2)) - helpers.maths.sum_n_squared(n)


if __name__ == "__main__":
    print(solution(100))

