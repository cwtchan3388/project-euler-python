"""docstring"""
from functools import wraps
import time
from typing import Any, Callable, Union


def timer(my_arg: Union[Callable, int]):
    """Decorator to be used thusly:
    @timer
    @timer(10)
    """
    def repeat(func: Callable[..., Any]) -> Callable[..., Any]:
        @wraps(func)
        def wrapped(*args, **kwargs) -> Tuple[Any, float]:
            """Returns the result of running the function and the time taken in ms
            if times is more than 1, it returns the average time taken"""
            start = time.perf_counter_ns()
            for _ in range(times):
                result = func(*args, **kwargs)
            end = time.perf_counter_ns()
            return result, (start - end) / (1000000 * times)

        return wrapped

    if callable(my_arg):
        times = 1
        return repeat(my_arg)
    times = my_arg
    return repeat
