"""helpers"""
import math


def is_prime(num: int) -> bool:
    """
    calculates if something is a prime
    :param num: the number to check
    :returns: if the number is a prime
    """
    if num <= 3:
        return num > 1
    if num & 1 or num % 3 == 0:
        return False
    for i in range(5, math.isqrt(num) + 1, 6):  # we rely the mutability of factors
        if num % i == 0 or num % (i + 2) == 0:
            return False
    return True


def sum_n(n: int) -> int:
    """Calculates the sum of natural numbers from 1 to n"""
    return int((math.pow(n, 2) + n) / 2)


def sum_n_squared(n: int) -> int:
    """Calculates the sum of the squares of natural numbers from 1 to n"""
    return int(((2 * math.pow(n, 3)) + (3 * math.pow(n, 2)) + n) / 6)

