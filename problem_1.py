"""Multiples of 3 or 5"""
import time


def solution(num: int) -> int:
    """Find the sum of all the numbers below num that are divisible by 3 and 5
    :param num: The number to work up to
    :returns: the sum
    """
    total = 0
    for i in range(3, num):
        if i % 3 == 0 or i % 5 == 0:
            total += i

    return total


if __name__ == "__main__":
    start_time = time.time()
    answer = solution(1000)
    end_time = time.time()
    print(f"{answer} {1000 * (end_time - start_time):.3f}ms")

    start_time = time.time()
    answer = sum(filter(lambda x: x % 3 == 0 or x % 5 == 0, range(1000)))
    end_time = time.time()
    print(f"{answer} {1000 * (end_time - start_time):.3f}ms")
