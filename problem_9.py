"""Project Euler Problem 8, Special Pythagorean triplet
a^2 + b^2 = c^2, there exists only one triplet whereby
a + b + c == 1000"""
from functools import reduce

# mathsisfun.com/numbers/pythagorean-triples.html
# when m and n are two positive integers (m > n)
# a = m^2 - n^2
# b = 2mn
# c = m^2 + n^2
# therefore 2m^2 + 2mn = 1000
# m(m + n) = 500
# therefore m < sqrt(500)


def find_m_n() -> tuple[int, int]:
    """finds m and n"""
    m = 22
    found = False
    while m > 0 and not found:
        n = m - 1
        m -= 1
        while n > 0 and not found:
            if m * (m + n) == 500:
                found = True
                break
            n -= 1

    assert found
    return m, n

def get_pythagorean_triplet(m: int, n: int) -> tuple[int, int, int]:
    """finds pythagorean triplet from m and n"""
    return m**2 - n**2, 2*m*n, m**2 + n**2 

if __name__ == '__main__':
    m, n = find_m_n()
    print(m, n)
    a, b, c = get_pythagorean_triplet(m, n)
    print(a, b, c, a + b + c)

    print(get_pythagorean_triplet(*find_m_n()))
    print(reduce(lambda x,y : x*y, get_pythagorean_triplet(*find_m_n())))
