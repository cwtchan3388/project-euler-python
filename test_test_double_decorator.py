from stuff.timer_with_optional_arg import timer


TOLERANCE = 1e-6

@timer
def get_sqrt(x: int) -> float:
    for i in range(x):
        if i*i > x:
            break
    upper = i
    lower = i - 1
    mid = i + 0.5
    current_square = mid*mid
    while abs(current_square - x) > TOLERANCE:
        if current_square > x:
            upper = mid
            mid -= (lower - mid) / 2
        else:
            lower = mid
            mid += (upper - mid) / 2
        current_square = mid * mid
    return mid

# print(get_sqrt(10))


@timer
def ha():
    print("ha")

print(ha())

@timer(10)
def ho():
    print("ho")

print(ho())
