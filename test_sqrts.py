"""doc"""
from math import sqrt
from helpers.timers import timer


TIMES = 1000000

@timer(times=TIMES)
def math_sqrt(num: int) -> float:
    return sqrt(num)


@timer(times=TIMES)
def inverse_pow(num: int) -> float:
    return num ** 0.5


print(math_sqrt(13195))
print(inverse_pow(13195))
