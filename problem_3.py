"""Find largest prime factor"""
from math import sqrt
from typing import Union

from helpers.maths import is_prime
from helpers.timers import timer


def find_largest_factor(num: int) -> int:
    """finds the largest factors of num
    :param num: the number to find the factors of
    :yields: the current largest factor
    """
    for i in range(int(sqrt(num)), 1, -1):
        if num % i == 0:
            yield i


@timer(10)
def find_largest_prime_factor(num: int) -> Union[int, None]:
    """Find largest prime of num
    :param num: the number to find the largest prime of
    :returns: either the prime or None
    """
    for factor in find_largest_factor(num):
        if is_prime(factor):
            return factor
    return None


if __name__ == "__main__":
    solution, time_taken = find_largest_prime_factor(600851475143)
    print(f"{solution} {time_taken:.3f}ms")
