"""Find the sum of even fibonacci numbers"""


def solution(num: int) -> int:
    """Find the sum of even fibonacci numbers
    :param num: the fibonacci number to not exceed
    :returns: the sum
    """
    total = 0
    cur_num = 1
    prev_num = 1
    next_num = cur_num + prev_num
    while next_num < num:
        prev_num = cur_num
        cur_num = next_num
        next_num = cur_num + prev_num
        if cur_num % 2 == 0:
            total += cur_num

    return total


if __name__ == "__main__":
    print(solution(4000000))
