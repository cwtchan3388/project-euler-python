"""Find the largest palindromic number made from the product of 3-digit numbers"""
from helpers.timers import timer


MAX = 999
MIN = 900


def is_palindrome(num: int) -> bool:
    """docstring"""
    num_as_str = str(num)
    return num_as_str == num_as_str[::-1]


@timer(10)
def solution() -> int:
    """docstring"""
    cur_max = 0
    for i in range(MAX, MIN, -1):
        for j in range(MAX, MIN, -1):
            if is_palindrome(i*j):
                if i*j > cur_max:
                    cur_max = i*j

    return cur_max


if __name__ == "__main__":
    print(solution())
