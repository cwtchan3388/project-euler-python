"""Smallest multiple"""
from helpers.timers import timer


@timer(10)
def solution() -> int:
    """Find the smallest number which is evenly divisible by all numbers from
    1 to 20"""

    # If divisible by
    # 20 -> 10, 5, 4, 2
    # 18 -> 9, 6, 3
    # 16 -> 8, 4, 2
    # 15 -> 5, 3
    # 14 -> 7, 2
    # 12 -> 6, 4, 3, 2
    # The above numbers have covered all the other nums possible
    # we must include 19, 17, 13, 11
    # maybe try if divisble by primes first
    # then largest nums with most divisibles
    # then final set
    # 2520 works for nums 1..10, so we should start there
    # number has to be even
    # probably has to be a multiple of 20
    # there is actually a much faster way of doing this
    num = 2520
    primes = (11, 13, 17, 19)
    second = (20, 18, 16, 14)
    third = (15, 12)
    while True:
        num += 20
        if any(num % prime > 0 for prime in primes):
            continue
        if any(num % i > 0 for i in second):
            continue
        if any(num % i > 0 for i in third):
            continue
        return num


if __name__ == "__main__":
    print(solution())
